import axios from "axios";
import {
  popularGamesURL,
  upcomingGamesURL,
  newgamesURL,
  searchGameURL,
} from "../api";

// Action Creator
export const loadGames = () => async (dispatch) => {
  // FETCH AXION
  const popularData = await axios.get(popularGamesURL());
  const newGamesData = await axios.get(newgamesURL());
  const upcomingData = await axios.get(upcomingGamesURL());

  dispatch({
    type: "FETCH_GAMES",
    payload: {
      popular: popularData.data.results,
      upcoming: upcomingData.data.results,
      newgames: newGamesData.data.results,
    },
  });
};

export const fetchSearch = (game_name) => async (dispatch) => {
  const searchGames = await axios.get(searchGameURL(game_name));

  dispatch({
    type: "FETCH_SEARCHED",
    payload: {
      searched: searchGames.data.results,
    },
  });
};
